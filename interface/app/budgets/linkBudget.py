import logging
import math
from typing import Optional, Any

import utilities
from componentRegistry import ComponentRegistry
from components.antenna import AntennaComponent
from components.pointingComponent import PointingComponent
from components.trajectory import TrajectoryComponent
from components.transceiver import TransceiverComponent


class LinkBudget:
    """
    A calculator to get useful values related ot the link budget
    """

    atmosphericLosses = 0.1  # Atmospheric losses in dB
    frequency = 10e9  # Frequency in Hz
    groundStationDirectivity = 23  # Ground station directivity in dB

    minEbN0 = 10
    accEbN0 = 14
    maxEbN0 = 30

    def __init__(self, registry: ComponentRegistry):
        self.registry = registry

    def getTransmitPower(self):
        """
        Get transmit power in W
        """
        transceiver: Optional[TransceiverComponent] = self.registry.getSingleComponentByClass(TransceiverComponent)

        # Return this transceiver's TX power
        return transceiver.txPower * transceiver.efficiency if transceiver else 0

    def getReceivePower(self):
        """
        Get receive power at ground station in W
        """
        power = self.getTransmitPower() * self.__dbToPower(-self.getPathLossDb()) * (1 - self.getLinkLosses()) \
            * self.getDirectivityGain() * math.pow(10, self.groundStationDirectivity / 10)

        return power if power > 0 else 0

    def getDirectivityGain(self):
        """
        Get directivity gain in ratio
        """
        antenna = self.registry.getSingleComponentByClass(AntennaComponent)

        # Return this antenna's directivity gain
        return antenna.directivityGain if antenna else 1

    def getAtmosphericLosses(self):
        """
        Get atmospheric losses in ratio
        """
        return 1 - self.__dbToPower(- self.atmosphericLosses)

    def getMeanDistance(self):
        """
        Get the mean distance between the space and ground segments in m
        """
        # The selected trajectory
        trajectory = self.registry.getSingleComponentByClass(TrajectoryComponent)

        # Return the trajectory's mean altitude
        return trajectory.meanAltitude if trajectory else 1e3

    def getFrequency(self):
        """
        Get the transmission frequency of the satellite in Hz
        """
        return self.frequency

    def getWavelength(self):
        """
        Get the transmitted wavelength in m (assuming free space transmission)
        """
        return 3e8 / self.getFrequency()

    def getLatency(self):
        """
        Get the amount of time it takes for the signal to reach the earth from the space segment, in s
        """
        return self.getMeanDistance() / 3e8

    def getPathLossDb(self):
        """
        Get path loss in dB
        """
        return 10 * 2 * math.log10(4 * math.pi * self.getMeanDistance() / self.getWavelength())

    def getPathLoss(self):
        """
        Get path loss in W
        """
        # Calculate the path loss. As we are in free space, we assume a path loss exponent of 2.
        dB = self.getPathLossDb()

        logging.debug('Path loss is {} dB'.format(dB))

        return self.getTransmitPower() * (1 - self.__dbToPower(-dB))

    def getErrorRate(self):
        """
        Get error rate in ratio
        """
        if self.getReceivePower() == 0:
            return 1

        # System desired data rate in dbHz
        dataRate = 10 * math.log10(self.registry.getSingleComponentValue(TransceiverComponent, 'dataRate', 1) * 1e3)
        logging.debug('Data rate in dbHz {}'.format(dataRate))

        # Signal level at ground station in dbW
        signalLevel = 10 * math.log10(self.getReceivePower())

        # Boltzman's Constant in dBW/K/Hz
        boltzman = -228.6

        # Ground station Figure of Merit (G/T)
        gsFom = - 10 * math.log10(489)

        # Ground station Signal-to-Noise Power Density
        gsSN0 = signalLevel - boltzman + gsFom

        # Final ebN0 for downlink
        ebN0 = gsSN0 - dataRate
        logging.debug('D/L EbN0 {}'.format(ebN0))

        # Calculate Lagrange polynomials for quadratic interpolation
        if ebN0 > self.maxEbN0:
            BER = 0
        elif ebN0 < self.minEbN0:
            BER = 1
        else:
            x0 = self.minEbN0
            x1 = self.accEbN0
            x2 = self.maxEbN0
            y0 = 0.9
            y1 = 0.1
            y2 = 0
            x = ebN0
            L0 = (x - x1) * (x - x2) / (x0 - x1) / (x0 - x2)
            L1 = (x - x0) * (x - x2) / (x1 - x0) / (x1 - x2)
            L2 = (x - x0) * (x - x1) / (x2 - x0) / (x2 - x1)

            BER = y0 * L0 + y1 * L1 + y2 * L2

        if BER < 0:
            BER = 0
        elif BER > 1:
            BER = 1

        return BER

    def getPointingLosses(self):
        """
        Get pointing losses due to ADCS
        """
        # The total pointing error in degrees (of all pointing components)
        totalError = self.registry.componentSum('pointingError', lambda c: issubclass(c.__class__, PointingComponent))

        # The selected antenna
        antenna = self.registry.getSingleComponentByClass(AntennaComponent)

        if not antenna:
            # There is no antenna
            return 0
        elif not antenna.directive:
            # The antenna is not directive, there is no pointing loss
            return 0
        else:
            if antenna.directivityGain > 6:
                # Parabolic dish
                phase = 6 * totalError
            else:
                # Patch
                phase = 6 / 7 * totalError

        if phase > 90:
            # The phase is larger than 90 degrees - this means maximum pointing loss (sin becomes 1)
            phase = 90

        # Return the pointing loss as calculated with maths
        return math.sin(phase / 360 * 2 * math.pi)

    def getOtherLosses(self):
        """
        Get losses not belonging in one of the other categories
        """
        return utilities.lossSum(self.getLinkLosses(), -self.getPointingLosses(), -self.getAtmosphericLosses())

    def getLinkLosses(self):
        """
        Get total link losses (in ratio) - does not include the path loss
        """
        # Sum the link penalty of all components
        return utilities.lossSum(self.registry.componentLossSum('linkPenalty'), self.getAtmosphericLosses(), self.getPointingLosses())

    def __dbToPower(self, dB):
        """
        Convert a dB value to a power ratio
        """
        return math.pow(10, dB / 10)
