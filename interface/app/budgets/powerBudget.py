import math
from typing import Optional, Any

from componentRegistry import ComponentRegistry
from components.antenna import AntennaComponent
from components.batteryPack import BatteryPackComponent
from components.pointingComponent import PointingComponent
from components.solarPanel import SolarPanelComponent
from components.trajectory import TrajectoryComponent
from components.transceiver import TransceiverComponent


class PowerBudget:
    """
    A calculator to get useful values related ot the power budget
    """

    def __init__(self, registry: ComponentRegistry):
        self.registry = registry

    def getSolarPower(self):
        """
        Get the received solar power in W/m^2
        """
        return self.registry.getSingleComponentValue(TrajectoryComponent, 'solarPower', 0)

    def getSolarArea(self):
        """
        Get the effective area of the solar panels in m^2
        """
        return 30 * 10 * 1e-4

    def getSolarEfficiency(self):
        """
        Get the efficiency of the solar panels in ratio
        """
        return self.registry.getSingleComponentValue(SolarPanelComponent, 'efficiency', 1)

    def getAvailablePower(self):
        """
        Get the average available solar power in W
        """
        return self.getSolarArea() * self.getSolarPower() * self.getSolarEfficiency()

    def getPowerStorage(self):
        """
        Get the stored power in J
        """
        return self.registry.componentClassSum(BatteryPackComponent, 'storedEnergy') * 3600

    def getEclipseDuration(self):
        """
        Get the maximum duration of an eclipse in s
        """
        return self.registry.getSingleComponentValue(TrajectoryComponent, 'eclipseDuration', 0)

    def getRequiredPower(self):
        """
        Get the average required power from active components in W
        """
        return self.registry.componentSum('powerAverage')

    def getRequiredEnergyInEclipse(self):
        """
        Get the required energy for the duration of an eclipse
        """
        return self.getRequiredPower() * self.getEclipseDuration()
