import utilities
from budgets.linkBudget import LinkBudget
from componentRegistry import ComponentRegistry
from components.payloadSensor import PayloadSensorComponent
from components.trajectory import TrajectoryComponent
from components.transceiver import TransceiverComponent


class DataBudget:
    """
    A calculator to get useful values related to the data budget
    """

    def __init__(self, registry: ComponentRegistry, linkBudget: LinkBudget):
        self.registry = registry
        self.linkBudget = linkBudget

    def getMinimumData(self):
        """
        Get the minimum data to be transferred in bytes
        """
        return self.registry.componentClassSum(PayloadSensorComponent, 'dataRequired')

    def getCommsWindow(self):
        """
        Get the communications window in seconds
        """
        return self.registry.getSingleComponentValue(TrajectoryComponent, 'commsWindow', 0)

    def getPenalty(self):
        """
        Get the total penalty of all components in ratio
        """
        return utilities.lossSum(self.registry.componentLossSum('dataPenalty'), 2 * self.linkBudget.getErrorRate())

    def getDataRate(self):
        """
        Get the transceiver data rate in bps
        """
        return self.registry.getSingleComponentValue(TransceiverComponent, 'dataRate', 0) * 1e3

    def getDataTransferred(self):
        """
        Get the total transferred data in bytes
        """
        return self.getDataRate() / 8 * self.getCommsWindow() * (1 - self.getPenalty())
