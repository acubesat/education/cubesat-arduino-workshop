import logging
import math
import random
import threading

import serial

port = ""
dataLock = threading.Lock()  # A lock for access to the data
data = {}  # Interesting data from the sensors
stopEvent = threading.Event()  # An event to signify a signal to stop this thread

txParamLock = threading.Lock()  # A lock for access to the txParameters
txParameters = {}  # TX parameters for the NRF24L01+
rewriteParamsEvent = threading.Event()  # An event to signify that we need to resend TX parameters via Serial



def work():
    """
    Work function of a thread devoted to serial communication with a microcontroller
    """
    logging.basicConfig(level=logging.DEBUG)

    logging.debug("Started serial work function")

    with serial.Serial(port, 115200, timeout=1) as ser:
        while not stopEvent.is_set():
            line = ser.readline()

            if len(line) > 0:
                # Serial data exists
                dataLock.acquire()

                command = int(line[0])
                try:
                    value = float(line[1:])
                except:
                    value = 0

                if command == ord('t'):
                    data["Temperature"] = value
                elif command == ord('b'):
                    data["Brightness"] = value
                elif command == ord('i'):
                    data["Infrared"] = value
                elif command == ord('u'):
                    data["Ultrasonic"] = value

                # do process...
                dataLock.release()

            if rewriteParamsEvent.is_set():
                txParamLock.acquire()

                # Here we send the parameters
                ser.write(b"p" + str.encode(str(txParameters["power"])) + b"\n")
                ser.write(b"d" + str.encode(str(txParameters["dataRate"])) + b"\n")

                txParamLock.release()
                rewriteParamsEvent.clear()

            data["Random number"] = random.uniform(1, 10)

            logging.debug('Serial loop')
        stopEvent.clear()
# ...     x = ser.read()          # read one byte
# ...     s = ser.read(10)        # read up to ten bytes (timeout)
# ...     line = ser.readline()   # read a '\n' terminated line
