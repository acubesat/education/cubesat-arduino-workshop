from enum import Enum


class Subs(Enum):
    PAYLOAD = 1
    TRA = 2
    EPS = 3
    COMMS = 4
    ADCS = 5
    OBC = 6
    THE = 7
    STR = 8
