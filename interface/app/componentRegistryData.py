from component import Component
from components.antenna import AntennaComponent
from components.batteryPack import BatteryPackComponent
from components.microcontroller import MicrocontrollerComponent
from components.payloadSensor import PayloadSensorComponent
from components.pointingActuatorComponent import PointingActuatorComponent
from components.pointingComponent import PointingComponent
from components.pointingSensorComponent import PointingSensorComponent
from components.solarPanel import SolarPanelComponent
from components.trajectory import TrajectoryComponent
from components.transceiver import TransceiverComponent
from subsystem import Subs


def initialize(registry):
    """
    Add all available satellite components to an array
    """

    # Various components not playing a role in the design
    component = Component(Subs.STR, "3U CubeSat Structure")
    component.costPenalty = 3000
    component.required = True
    registry.append(component)

    component = Component(Subs.STR, "Launch")
    component.costPenalty = 3000
    component.required = True
    registry.append(component)

    # TRA components
    component = TrajectoryComponent(Subs.TRA, "LEO ISS")
    component.meanAltitude = 400e3
    component.solarPower = 28.794 / (30 * 10 * 1e-4)
    component.commsWindow = 1185.463 * 60
    component.eclipseDuration = 17.622 * 60
    component.isLEO = True
    registry.append(component)

    component = TrajectoryComponent(Subs.TRA, "LEO Polar")
    component.meanAltitude = 600e3
    component.solarPower = 27.665 / (30 * 10 * 1e-4)
    component.commsWindow = 6400.432 * 60
    component.eclipseDuration = 22.325 * 60
    component.isLEO = True
    registry.append(component)

    component = TrajectoryComponent(Subs.TRA, "Geosynchronous")
    component.meanAltitude = 25000e3
    component.solarPower = 20.755 / (30 * 10 * 1e-4)
    component.commsWindow = 1358643.077 * 60
    component.eclipseDuration = 49.181 * 60
    component.isLEO = False
    registry.append(component)

    # ADCS components
    component = PointingComponent(Subs.ADCS, "Gyroscope")
    component.costPenalty = 75
    component.powerPenalty = 0.02
    component.pointingError = 2.5
    component.required = True
    registry.append(component)

    component = PointingSensorComponent(Subs.ADCS, "Magnetometer")
    component.costPenalty = 40
    component.powerPenalty = 0.02
    component.pointingError = 1
    registry.append(component)

    component = PointingSensorComponent(Subs.ADCS, "Sun Sensor")
    component.costPenalty = 3600
    component.powerPenalty = 0.07
    component.pointingError = 0.2
    registry.append(component)

    component = PointingSensorComponent(Subs.ADCS, "Earth Sensor")
    component.costPenalty = 6000
    component.powerPenalty = 0.132
    component.pointingError = 0.2
    registry.append(component)

    component = PointingSensorComponent(Subs.ADCS, "Star Tracker")
    component.costPenalty = 30000
    component.powerPenalty = 0.65
    component.pointingError = 0.009
    registry.append(component)

    component = PointingActuatorComponent(Subs.ADCS, "Gravity Gradient")
    component.pointingError = 90
    registry.append(component)

    component = PointingActuatorComponent(Subs.ADCS, "3axis - Magnetorquers")
    component.costPenalty = 8000
    component.powerPenalty = 0.7
    component.pointingError = 10
    registry.append(component)

    component = PointingActuatorComponent(Subs.ADCS, "3axis - Magnetorquers, 1axis - Reaction Wheel")
    component.costPenalty = 12000
    component.powerPenalty = 1.2
    component.pointingError = 3
    registry.append(component)

    component = PointingActuatorComponent(Subs.ADCS, "3axis - Magnetorquers, 3axis - Reaction Wheel")
    component.costPenalty = 20000
    component.powerPenalty = 1.9
    component.pointingError = 0.1
    registry.append(component)

    # COMMS components
    component = AntennaComponent(Subs.COMMS, "Dipole Antenna")
    component.costPenalty = 100
    component.directivityGain = 2
    component.directive = False
    registry.append(component)

    component = AntennaComponent(Subs.COMMS, "Patch Antenna")
    component.costPenalty = 2500
    component.directive = True
    component.directivityGain = 4
    registry.append(component)

    component = AntennaComponent(Subs.COMMS, "Parabolic Dish Antenna")
    component.costPenalty = 22000
    component.directive = True
    component.directivityGain = 200
    registry.append(component)

    component = TransceiverComponent(Subs.COMMS, "Transceiver")
    component.costPenalty = 10
    component.required = True
    registry.append(component)

    # EPS
    component = SolarPanelComponent(Subs.EPS, "Panel 1")
    component.costPenalty = 30000
    component.efficiency = 0.295
    registry.append(component)

    component = SolarPanelComponent(Subs.EPS, "Panel 2")
    component.costPenalty = 25000
    component.efficiency = 0.25
    registry.append(component)

    component = SolarPanelComponent(Subs.EPS, "Panel 3")
    component.costPenalty = 20000
    component.efficiency = 0.20
    registry.append(component)

    component = BatteryPackComponent(Subs.EPS, "1 Battery Pack")
    component.costPenalty = 2500
    component.storedEnergy = 10.2
    registry.append(component)

    component = BatteryPackComponent(Subs.EPS, "2 Battery Packs")
    component.costPenalty = 2500 * 2
    component.storedEnergy = 10.2 * 2
    registry.append(component)

    component = BatteryPackComponent(Subs.EPS, "3 Battery Packs")
    component.costPenalty = 2500 * 3
    component.storedEnergy = 10.2 * 3
    registry.append(component)

    # OBC
    component = MicrocontrollerComponent(Subs.OBC, "Microcontroller")
    component.costPenalty = 10
    component.powerPenalty = 1
    component.maxDataPenalty = 0.95
    registry.append(component)

    component = MicrocontrollerComponent(Subs.OBC, "Low-Power Microcontroller")
    component.costPenalty = 20
    component.powerPenalty = 0.1
    component.maxDataPenalty = 0.951
    registry.append(component)

    component = MicrocontrollerComponent(Subs.OBC, "Radiation-Hardened Microcontroller")
    component.costPenalty = 1000
    component.powerPenalty = 0.5
    component.maxDataPenalty = 0.2
    registry.append(component)

    component = MicrocontrollerComponent(Subs.OBC, "Radiation-Hardened Triple-Redundant OBC")
    component.costPenalty = 8000
    component.powerPenalty = 3
    component.maxDataPenalty = 0.01
    registry.append(component)

    # Payload
    component = PayloadSensorComponent(Subs.PAYLOAD, "Radiometer")
    component.costPenalty = 5000
    component.powerPenalty = 6
    component.powerDutyCycle = 0.5
    component.dataRequired = 1e9
    registry.append(component)

    component = PayloadSensorComponent(Subs.PAYLOAD, "Thermal Camera")
    component.costPenalty = 23000
    component.powerPenalty = 1
    component.powerDutyCycle = 0.5
    component.dataRequired = 4e9
    registry.append(component)

    component = PayloadSensorComponent(Subs.PAYLOAD, "Optical Sensor")
    component.costPenalty = 16000
    component.powerPenalty = 1
    component.powerDutyCycle = 0.5
    component.dataRequired = 8e9
    registry.append(component)
