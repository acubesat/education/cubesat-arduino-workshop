from PyQt5.QtCore import Qt, QAbstractTableModel, QModelIndex
from PyQt5.QtGui import QColor

from componentRegistry import ComponentRegistry


class PowerTableModel(QAbstractTableModel):
    registry: ComponentRegistry

    def __init__(self, registry):
        QAbstractTableModel.__init__(self)
        self.registry = registry
        self.componentList = []

    def updateComponentList(self):
        """
        Update the component list based on the registry. Call this whenever the registry is updated, and before updating
        this table
        """
        # Update only components with a non-zero cost
        self.componentList = list(self.registry.getActiveComponents(lambda c: c.call('powerAverage') > 0))

        # Notify Qt that the table data has been updated
        self.dataChanged.emit(self.createIndex(0, 0), self.createIndex(self.rowCount(), self.columnCount()))

    def rowCount(self, parent=QModelIndex()):
        return 100

    def columnCount(self, parent=QModelIndex()):
        return 3

    def headerData(self, section, orientation, role=None):
        if role != Qt.DisplayRole:
            return None
        if orientation == Qt.Horizontal:
            return ("Component", "Maximum Power", "Average Power")[section]
        else:
            return "{}".format(section)

    def data(self, index, role=Qt.DisplayRole):
        column = index.column()
        row = index.row()

        if role == Qt.DisplayRole:
            if row >= len(self.componentList):
                return None
            elif column == 0:
                component = self.componentList[row].displayName
                return component
            elif column == 1:
                return "{:.2f} W".format(self.componentList[row].call('powerPenalty'))
            elif column == 2:
                return "{:.2f} W".format(self.componentList[row].call('powerAverage'))
        elif role == Qt.BackgroundRole:
            return QColor(Qt.white)
        elif role == Qt.TextAlignmentRole:
            return Qt.AlignRight

        return None
