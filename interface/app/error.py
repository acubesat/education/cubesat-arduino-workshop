class Error:
    """
    An error in the design. The existence of errors means that a team is invalidated on the spot!
    """
    def __init__(self, description, component=None) -> None:
        self.description = description
        self.component = component
