from component import Component


class SolarPanelComponent(Component):
    """
    A component representing a solar panel
    """

    exclusive = True
    enforced = True

    def __init__(self, *args, **kwargs):
        super(SolarPanelComponent, self).__init__(*args, **kwargs)  # Call parent constructor
        self.area = 0  # The solar panel area in cm^2
        self.efficiency = 1  # The solar panel efficiency in ratio
