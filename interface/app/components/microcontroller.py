from component import Component
from componentRegistry import ComponentRegistry
from components.trajectory import TrajectoryComponent


class MicrocontrollerComponent(Component):
    """
    A component representing a microcontroller
    """

    exclusive = True
    enforced = True

    minSEEAltitude = 100  # Altitude (in km) where the data penalty is 0
    maxSEEAltitude = 2000  # Altitude (in km) where the data penalty is self.maxDataPenalty

    def __init__(self, *args, **kwargs):
        super(MicrocontrollerComponent, self).__init__(*args, **kwargs)  # Call parent constructor
        self.maxDataPenalty = 0

    def __dataPenalty(self, registry: ComponentRegistry):
        # The altitude of the trajectory in km
        altitude = registry.getSingleComponentValue(TrajectoryComponent, 'meanAltitude', 300e3) / 1e3

        # Linearly interpolate between the min and max Single Event Effect altitudes
        if altitude > self.maxSEEAltitude:
            return self.maxDataPenalty
        elif altitude < self.minSEEAltitude:
            return 0
        else:
            return (altitude - self.minSEEAltitude) * self.maxDataPenalty / (self.maxSEEAltitude - self.minSEEAltitude)

    def unpickle(self):
        super().unpickle()
        self.dataPenalty = self.__dataPenalty

