from component import Component


class BatteryPackComponent(Component):
    """
    A component representing a battery pack
    """

    exclusive = True
    enforced = True

    def __init__(self, *args, **kwargs):
        super(BatteryPackComponent, self).__init__(*args, **kwargs)  # Call parent constructor
        self.storedEnergy = 0  # stored energy in Watt hours
