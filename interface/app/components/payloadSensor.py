from component import Component


class PayloadSensorComponent(Component):
    """
    A component representing a payload sensor
    """

    exclusive = True
    enforced = True

    def __init__(self, *args, **kwargs):
        super(PayloadSensorComponent, self).__init__(*args, **kwargs)  # Call parent constructor
        self.dataRequired = 0  # required data to transmit in bytes
