from component import Component
from componentRegistry import *
from components.antenna import AntennaComponent


class PointingComponent(Component):
    """
    A component representing a communications antenna
    """

    def __init__(self, *args, **kwargs):
        super(PointingComponent, self).__init__(*args, **kwargs)  # Call parent constructor
        self.maxLinkPenalty = 0
        self.pointingError = 0  # Pointing loss in degrees
    
    def __linkPenalty(self, registry: ComponentRegistry):
        # Get the currently selected antenna
        antenna = registry.getSingleComponentByClass(AntennaComponent)

        # Provide link penalty only if the antenna is directive
        return self.maxLinkPenalty if antenna and antenna.directive else 0

    def unpickle(self):
        super().unpickle()
        self.linkPenalty = self.__linkPenalty


