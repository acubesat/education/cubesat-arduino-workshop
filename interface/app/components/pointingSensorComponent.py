from componentRegistry import ComponentRegistry
from components.pointingComponent import PointingComponent
from components.trajectory import TrajectoryComponent
from error import Error


class PointingSensorComponent(PointingComponent):
    def constraints(self, registry: ComponentRegistry):
        yield from super().constraints(registry)

        if self.name == "Magnetometer" and not registry.getSingleComponentValue(TrajectoryComponent, 'isLEO', False):
            yield Error("The Magnetometer only works on Low Earth Orbits")
        elif self.name == "Earth Sensor" and registry.getSingleComponentValue(TrajectoryComponent, 'isLEO', False):
            yield Error("The Earth Sensor cannot work on Low Earth Orbit")
