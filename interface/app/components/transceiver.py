from component import Component
from componentRegistry import ComponentRegistry
from error import Error


class TransceiverComponent(Component):
    """
    A component representing a communications transceiver
    """

    def __init__(self, *args, **kwargs):
        super(TransceiverComponent, self).__init__(*args, **kwargs)  # Call parent constructor
        self.dataRate = 9.6  # data rate in kbps
        self.txPower = 2.5  # electrical power in W
        self.efficiency = 0.5  # efficiency of the transceiver in ratio

    def __powerPenalty(self, registry: ComponentRegistry):
        return self.txPower

    def __powerDutyCycle(self, registry: ComponentRegistry):
        return 0.4

    def constraints(self, registry):
        if self.txPower <= 0:
            yield Error("The transceiver cannot transmit with a power of 0 W!", self)

    def unpickle(self):
        super().unpickle()
        self.powerPenalty = self.__powerPenalty
        self.powerDutyCycle = self.__powerDutyCycle




