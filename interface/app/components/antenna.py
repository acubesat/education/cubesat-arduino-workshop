from component import Component


class AntennaComponent(Component):
    """
    A component representing a communications antenna
    """

    exclusive = True
    enforced = True

    def __init__(self, *args, **kwargs):
        super(AntennaComponent, self).__init__(*args, **kwargs)  # Call parent constructor
        self.directive = False
        self.directivityGain = 1
