from component import Component


class TrajectoryComponent(Component):
    exclusive = True
    enforced = True

    """
    A component representing a satellite trajectory

    Do note that trajectories are not technically components :)
    """

    def __init__(self, *args, **kwargs):
        super(TrajectoryComponent, self).__init__(*args, **kwargs)  # Call parent constructor
        self.commsWindow = 0  # The communications window in seconds
        self.solarPower = 0  # The received solar power in Watts
        self.meanAltitude = 0  # The mean altitude in meters
        self.eclipseDuration = 0  # The maximum eclipse duration in seconds
        self.isLEO = False  # Whether this is a low earth orbit
