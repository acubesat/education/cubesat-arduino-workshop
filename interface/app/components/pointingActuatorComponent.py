from componentRegistry import ComponentRegistry
from components.pointingComponent import PointingComponent
from components.trajectory import TrajectoryComponent
from error import Error


class PointingActuatorComponent(PointingComponent):
    exclusive = True
    enforced = True
