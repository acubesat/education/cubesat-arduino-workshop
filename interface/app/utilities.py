import glob
import logging
import sys

import serial


def teamName():
    """
    Get the name of the team
    """
    return "Anatolia"

def trainingMode():
    """
    Get the operational mode of the software
    0: Everything is enabled
    1: Only 1st term
    2: Only 2nd term
    :return:
    """
    return 0

def lossSum(*values):
    """
    Sum the values, assuming they are loss ratios
    """
    sum = 1
    for value in values:
        # Throttle huge values
        if value > 1:
            value = 1
        elif value < -1:
            value = -1

        if value == 1:
            # A value is 1, this means the loss is 100%
            return 1
        elif value == 0:
            # No loss, do nothing
            pass
        elif value < 0:
            # Negative value
            value = -value  # make it positive
            sum /= (1 - value)  # remove the loss from the sum
        else:
            # Positive value, just add it
            sum *= (1 - value)

    sum = 1 - sum  # invert the sum again, since we inverted above

    if sum < 0:
        # Can't have negative losses
        return 0
    else:
        return sum


def list_serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result
