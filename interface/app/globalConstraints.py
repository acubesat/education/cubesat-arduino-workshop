import componentRegistry
from budgets.dataBudget import DataBudget
from budgets.linkBudget import LinkBudget
from budgets.powerBudget import PowerBudget
from components.pointingSensorComponent import PointingSensorComponent
from error import Error


def global_constraints(registry: componentRegistry.ComponentRegistry, powerBudget: PowerBudget, linkBudget: LinkBudget, dataBudget: DataBudget):
    """
    Yields Errors by calculating across all components
    """
    # ADCS, at least 2 sensors
    if registry.countActiveComponentsByClass(PointingSensorComponent) < 2:
        yield Error("You need to select at least 2 sensors for position determination")

    # Comms, data budget must be good
    if dataBudget.getDataTransferred() < dataBudget.getMinimumData():
        yield Error("Your data budget does not cover all the data that needs to be transferred")

    # Comms, error rate must be less than 15%
    if linkBudget.getErrorRate() > 0.15:
        yield Error("The error rate in the link budget must be less than 15% - otherwise, the data cannot be decoded!")

    # EPS, average power must be enough
    if powerBudget.getAvailablePower() < powerBudget.getRequiredPower():
        yield Error("Your power budget does not cover all the power that needs to be consumed")

    # EPS, batteries must have enough power to live during eclipse
    if powerBudget.getPowerStorage() < powerBudget.getRequiredEnergyInEclipse():
        yield Error("Your batteries do not have enough energy to survive during solar eclipse")
