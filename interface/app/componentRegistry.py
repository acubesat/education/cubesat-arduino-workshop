import jsonpickle

from component import *
from subsystem import Subs


class ComponentRegistry:
    """
    The registry that contains all (selected and unselected) components available for the design
    """

    def __init__(self):
        self.registry = []
        self.errors = []

    def get(self, name):
        """
        Gets a single (active or inactive) component by name
        """
        return next((x for x in self.registry if x.name == name), None)

    def getSingleComponentByClass(self, componentClass):
        """
        Returns the first active component that is of a specified class
        :param componentClass: The component class
        :return: The component, or None if not found
        """
        return next((x for x in self.registry if issubclass(x.__class__, componentClass) and x.active), None)

    def getSingleComponentValue(self, componentClass, attribute, default):
        """
        Returns a value of the first active component that is of a specified class
        :param componentClass: The component class
        :param attribute: The attribute to search, as a string
        :param default: The default value to return, if the component is not found
        """
        component = self.getSingleComponentByClass(componentClass)

        return component.call(attribute) if component else default

    def componentSum(self, column, predicate=None):
        """
        Get the sum of an attribute of a component, across all active components
        :param predicate: An additional predicate to test each component before including it in the sum
        """
        sum = 0
        for component in self.registry:
            # Validate if the predicate and the component is active
            if component.active and (predicate is None or predicate(component)):
                sum += component.call(column, self)
        return sum

    def componentClassSum(self, componentClass, column, predicate=None):
        """
        Get the sum of an attribute of a component, across all active components of a class
        :param predicate: An additional predicate to test each component before including it in the sum
        :param componentClass: The class of the component to search
        """
        if predicate is not None:
            return self.componentSum(column, lambda c: (issubclass(c.__class__, componentClass) and predicate(c)))
        else:
            return self.componentSum(column, lambda c: issubclass(c.__class__, componentClass))

    def componentLossSum(self, column, predicate=None):
        """
        Get the loss sum of an attribute of a component, across all active components
        The loss sum is defined as the result of losses of components in series, e.g. two components with 20% loss have
        a total 36% loss
        :param predicate: An additional predicate to test each component before including it in the sum
        """
        sum = 1
        for component in self.registry:
            # Validate if the predicate and the component is active
            if component.active and (predicate is None or predicate(component)):
                sum *= (1 - component.call(column, self))
        return 1 - sum

    def deactivateAll(self):
        """
        Deactivate all components (other than the required ones)
        """
        for component in self.registry:
            if not component.required:
                # A component is not required, deactivate it
                component.active = False
            else:
                # A component is required. It must be active at all times
                component.active = True

    def getActiveComponents(self, predicate=None):
        """
        Get a list of all the active components
        :param predicate: A predicate that needs to be true for any component returned. Note that components still have
        to be active to get on this list.
        """
        if predicate is not None:
            for component in self.registry:
                if component.active and predicate(component):
                    yield component
        else:
            for component in self.registry:
                if component.active:  # Do not perform any further checks
                    yield component

    def countActiveComponents(self, predicate=None):
        """
        Count the number of all active components
        :param predicate: A predicate that needs to be true for any component to be counted. Not that components still
        have to be active to get counted, regardless of the predicate.
        """
        if predicate is not None:
            return sum(c.active and predicate(c) for c in self.registry)
        else:
            return sum(c.active for c in self.registry)

    def getActiveComponentsByClass(self, componentClass, predicate=None):
        """
        Get the active components in a class
        :param componentClass: The component class
        :param predicate: A predicate that needs to be true for any component to be counted. Not that components still
        have to be active to get counted, regardless of the predicate.
        """
        if predicate is not None:
            return self.getActiveComponents(lambda c: (issubclass(c.__class__, componentClass) and predicate(c)))
        else:
            return self.getActiveComponents(lambda c: issubclass(c.__class__, componentClass))

    def countActiveComponentsByClass(self, componentClass, predicate=None):
        """
        Count the number of all active components in a class
        :param componentClass: The component class
        :param predicate: A predicate that needs to be true for any component to be counted. Not that components still
        have to be active to get counted, regardless of the predicate.
        """
        if predicate is not None:
            return self.countActiveComponents(lambda c: (issubclass(c.__class__, componentClass) and predicate(c)))
        else:
            return self.countActiveComponents(lambda c: issubclass(c.__class__, componentClass))

    def refresh(self):
        """
        Refresh some values that depend on the activated components
        """
        # Refresh the errors based on the component constraints
        self.errors = []

        # Get each component's errors
        for component in self.getActiveComponents():
            # Append the component's errors to our errors
            self.errors.extend(component.constraints(self))

    def export(self) -> str:
        """
        Serialize the registry and get it as a string
        """
        return jsonpickle.encode(self.registry)

    def infuse(self, pickle: str):
        """
        Unserialize the registry based on a string that is the result of an export
        """
        self.registry = jsonpickle.decode(pickle)

        # Unpickle all serialized components
        for component in self.registry:
            component.unpickle()
