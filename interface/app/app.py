#!/usr/bin/env python3
import logging
import sys
import threading

from PyQt5.QtCore import QTimer, QStringListModel
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QDoubleSpinBox, QComboBox, QFileDialog

import componentRegistryData
import globalConstraints
import serialThread
import utilities
from budgets.dataBudget import DataBudget
from budgets.linkBudget import LinkBudget
from budgets.powerBudget import PowerBudget
from component import Component
from components.transceiver import TransceiverComponent
from mainWindow import *
from costTableModel import *
from powerTableModel import PowerTableModel

print('Hello Python {}.{}.{}'.format(*sys.version_info))
logging.basicConfig(level=logging.DEBUG)


class AppWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Initialize the serial thread
        self.serialThread = None
        timer = QTimer(self)
        timer.timeout.connect(self.updatePeriodic)
        timer.start(100)
        self.sensor_model = QStandardItemModel(5, 2)
        self.ui.tableView_sensors.setModel(self.sensor_model)

        # Initialize components
        self.component_registry = ComponentRegistry()
        componentRegistryData.initialize(self.component_registry.registry)

        # Initialize UI
        self.buttonToComponent = {}
        self.componentToButton = {}
        self.linkComponentButtons()
        self.connectAll(self.ui.tabWidget_components)
        self.ui.actionExport.triggered.connect(self.exportAction)
        self.ui.actionImport.triggered.connect(self.importAction)
        self.ui.actionRefresh.triggered.connect(self.refreshAction)
        self.ui.actionConnect.triggered.connect(self.connectAction)
        self.refreshAction()

        # Disable tabs based on training mode
        if utilities.trainingMode() == 1:
            self.ui.tab_obc.setDisabled(True)
            self.ui.tab_adcs.setDisabled(True)
            self.ui.tab_comms.setDisabled(True)
        elif utilities.trainingMode() == 2:
            self.ui.tab_payload.setDisabled(True)
            self.ui.tab_eps.setDisabled(True)
            self.ui.tab_trajectory.setDisabled(True)

        # Initialize team-specific stuff
        self.ui.label_team.setText(utilities.teamName())

        # Initialize budgets
        self.cost_model = CostTableModel(self.component_registry)
        self.cost_model.updateComponentList()
        self.ui.cost_table.setModel(self.cost_model)

        self.power_model = PowerTableModel(self.component_registry)
        self.power_model.updateComponentList()
        self.ui.power_table.setModel(self.power_model)

        self.linkBudget = LinkBudget(self.component_registry)
        self.powerBudget = PowerBudget(self.component_registry)
        self.dataBudget = DataBudget(self.component_registry, self.linkBudget)

        self.readComponents()

        self.show()

    def linkComponentButtons(self):
        # TRA
        self.linkComponentButton("LEO ISS", self.ui.tra_iss)
        self.linkComponentButton("LEO Polar", self.ui.tra_polar)
        self.linkComponentButton("Geosynchronous", self.ui.tra_geo)

        # ADCS
        self.linkComponentButton("Magnetometer", self.ui.adcs_magnetometer)
        self.linkComponentButton("Sun Sensor", self.ui.adcs_sun_sensor)
        self.linkComponentButton("Earth Sensor", self.ui.adcs_earth_sensor)
        self.linkComponentButton("Star Tracker", self.ui.adcs_star_tracker)
        self.linkComponentButton("Gravity Gradient", self.ui.adcs_gg)
        self.linkComponentButton("3axis - Magnetorquers", self.ui.adcs_mtq)
        self.linkComponentButton("3axis - Magnetorquers, 1axis - Reaction Wheel", self.ui.adcs_rw1)
        self.linkComponentButton("3axis - Magnetorquers, 3axis - Reaction Wheel", self.ui.adcs_rw3)

        # Comms
        self.linkComponentButton("Dipole Antenna", self.ui.comms_dipole)
        self.linkComponentButton("Patch Antenna", self.ui.comms_patch)
        self.linkComponentButton("Parabolic Dish Antenna", self.ui.comms_parabolic)

        # EPS
        self.linkComponentButton("Panel 1", self.ui.eps_panel1)
        self.linkComponentButton("Panel 2", self.ui.eps_panel2)
        self.linkComponentButton("Panel 3", self.ui.eps_panel3)
        self.linkComponentButton("1 Battery Pack", self.ui.eps_battery1)
        self.linkComponentButton("2 Battery Packs", self.ui.eps_battery2)
        self.linkComponentButton("3 Battery Packs", self.ui.eps_battery3)

        # OBC
        self.linkComponentButton("Microcontroller", self.ui.obc_mcu)
        self.linkComponentButton("Low-Power Microcontroller", self.ui.obc_lp)
        self.linkComponentButton("Radiation-Hardened Microcontroller", self.ui.obc_rh)
        self.linkComponentButton("Radiation-Hardened Triple-Redundant OBC", self.ui.obc_tmr)

        # Payload
        self.linkComponentButton("Radiometer", self.ui.su_radio)
        self.linkComponentButton("Thermal Camera", self.ui.su_thermal)
        self.linkComponentButton("Optical Sensor", self.ui.su_optical)

    def connectAll(self, widget):
        """
        Connect the widget and its children with the update functions
        """
        if widget.__class__ == QPushButton:
            widget.clicked.connect(self.createButtonPresser(widget, self.buttonToComponent[widget]))
        elif widget.__class__ == QDoubleSpinBox:
            widget.valueChanged.connect(self.readComponents)
        elif widget.__class__ == QComboBox:
            widget.currentTextChanged.connect(self.readComponents)
        for child in widget.children():
            self.connectAll(child)

    def createButtonPresser(self, button, component):
        """
        Create a handler to parse button presses
        """
        return lambda pressed: (self.buttonPressed(button, pressed), self.readComponents())

    def buttonPressed(self, button: QPushButton, pressed=True):
        """
        A function that gets called whenever a button is pressed, in conjunction to self.readComponents
        """
        # Get the component corresponding to this button
        component = self.buttonToComponent[button]

        logging.info("Pressed the button for {}".format(component.displayName))

        # Find if the class of the component is exclusive
        if component.exclusive:
            # Find all the buttons belonging in other components of the same class
            tempButton: QPushButton
            tempComponent: Component
            for tempButton, tempComponent in self.buttonToComponent.items():
                # Catch buttons of the same class
                if tempComponent.__class__ == component.__class__ and tempComponent != component:
                    tempButton.setChecked(False)

        # A button was unpressed but it is not allowed
        if component.enforced and not pressed:
            button.setChecked(True)

    def readComponents(self):
        """
        Function that gets called whenever the user makes any modification in the UI. It updates the components
        registry based on the user's selections
        """
        self.component_registry.deactivateAll()

        # Read all the buttons
        for button, component in self.buttonToComponent.items():
            # Update the component value based on whether the corresponding button is pressed
            component.active = button.isChecked()

        # Parse the generic COMMs values
        transceiver: TransceiverComponent = self.component_registry.get("Transceiver")
        transceiver.active = True
        transceiver.txPower = self.ui.comms_txpower.value()
        # Parse the data rate
        dataRate = self.ui.comms_dataRate.currentText()
        if dataRate[-4:] == 'Mbps':
            transceiver.dataRate = 1000 * float(dataRate[:-4])
        else:
            transceiver.dataRate = float(dataRate[:-4])

        # Finally, update the budgets
        self.updateBudgets()

        import json

    def linkComponentButton(self, componentName, button: QPushButton):
        """
        Store a button-component link
        """
        self.buttonToComponent[button] = self.component_registry.get(componentName)
        self.componentToButton[self.buttonToComponent[button]] = button

    def updateBudgets(self):
        """
        Function that gets called whenever the budgets are updated.
        """
        self.ui.label_transmit_power.setText("{:.2f} W".format(self.linkBudget.getTransmitPower()))
        self.ui.label_directivity.setText("{:.0f} %".format(self.linkBudget.getDirectivityGain() * 100))
        self.ui.label_path_loss.setText("{:.5f} W".format(self.linkBudget.getPathLoss()))
        self.ui.label_atmo_loss.setText("{:.2f} %".format(self.linkBudget.getAtmosphericLosses() * 100))
        self.ui.label_pointing_loss.setText("{:.2f} %".format(self.linkBudget.getPointingLosses() * 100))
        self.ui.label_various_loss.setText("{:.2f} %".format(self.linkBudget.getOtherLosses() * 100))
        self.ui.label_receive_power.setText("{:,.3f} fW".format(self.linkBudget.getReceivePower() * 1e15))
        self.ui.label_error_rate.setText("{:.2f} %".format(self.linkBudget.getErrorRate() * 100))
        self.ui.label_ttt.setText("{:.4f} ms".format(self.linkBudget.getLatency() * 1e3))
        self.ui.label_ov_error.setText("{:.0f} %".format(self.linkBudget.getErrorRate() * 100))

        self.ui.label_required_data.setText("{:.2f} GB".format(self.dataBudget.getMinimumData() / 1e9))
        self.ui.label_comms_window.setText("{:,.0f} s".format(self.dataBudget.getCommsWindow()))
        self.ui.label_penalty.setText("{:.2f} %".format(self.dataBudget.getPenalty() * 100))
        self.ui.label_total_data.setText("{:.2f} GB".format(self.dataBudget.getDataTransferred() / 1e9))
        self.ui.label_ov_data.setText("{:.2f} GB / {:.0f} GB".format(self.dataBudget.getDataTransferred() / 1e9,
                                                                     self.dataBudget.getMinimumData() / 1e9))

        self.cost_model.updateComponentList()
        cost = self.component_registry.componentSum("costPenalty")
        self.ui.label_total_cost.setText("{:.2f} €".format(cost))
        self.ui.label_ov_cost.setText("{:,.0f} €".format(cost))

        self.power_model.updateComponentList()
        self.ui.label_solar_power.setText("{:.2f} W/m²".format(self.powerBudget.getSolarPower()))
        self.ui.label_solar_area.setText("{:.2f} cm²".format(self.powerBudget.getSolarArea() * 1e4))
        self.ui.label_average_power.setText("{:.2f} W".format(self.powerBudget.getAvailablePower()))
        self.ui.label_power_stored.setText("{:.2f} kJ".format(self.powerBudget.getPowerStorage() / 1e3))
        self.ui.label_eclipse_duration.setText("{:.2f} s".format(self.powerBudget.getEclipseDuration()))
        self.ui.label_required_power.setText("{:.2f} W".format(self.powerBudget.getRequiredPower()))
        self.ui.label_required_eclipse_energy.setText(
            "{:.2f} kJ".format(self.powerBudget.getRequiredEnergyInEclipse() / 1e3))
        self.ui.label_ov_power.setText(
            "{:.1f} W / {:.1f} W".format(self.powerBudget.getRequiredPower(), self.powerBudget.getAvailablePower()))

        self.component_registry.refresh()
        self.component_registry.errors.extend(globalConstraints.global_constraints(
            self.component_registry, dataBudget=self.dataBudget, linkBudget=self.linkBudget,
            powerBudget=self.powerBudget))
        self.ui.listWidget_constraints.clear()
        if self.component_registry.errors:
            self.ui.label_design_status.setText(
                "The design contains {} errors. Visit the \"Constraints\" tab to fix them!".format(
                    len(self.component_registry.errors)))
            self.ui.label_design_status.setStyleSheet("color: #f42")
            self.ui.tabs_budgets.setStyleSheet("QTabBar::tab:last { color: #f30 }")
            for error in self.component_registry.errors:
                self.ui.listWidget_constraints.addItem(error.description)
        else:
            self.ui.label_design_status.setText("The design is compliant with all constraints")
            self.ui.tabs_budgets.setStyleSheet("")
            self.ui.label_design_status.setStyleSheet("color: #2a2")

        # Send the updated values via serial
        self.updateSerialTXparameters()

    def connectAction(self):
        """
        Triggered when requested to connect/disconnect in a serial connection
        """
        try:
            if not self.serialThread or not self.serialThread.isAlive():
                self.ui.pushButton_serial_connect.setText("Disconnect")
                # The thread is not up, we need to set up a connection
                serialThread.port = self.ui.comboBox_serial.currentText()
                self.serialThread = threading.Thread(target=serialThread.work)
                self.serialThread.setDaemon(True)  # daemons will die when their parent is killed
                self.serialThread.start()
                self.ui.label_serial_status.setText("Connected")
            else:
                # The thread is awake, close it
                serialThread.stopEvent.set()
                self.ui.pushButton_serial_connect.setText("Connect")
                self.serialThread.join()
                self.ui.label_serial_status.setText("Disconnected")
        except RuntimeError as e:
            logging.error(e)


    def refreshAction(self):
        """
        Triggered when requested to refresh serial ports
        """
        # Clear all ports
        self.ui.comboBox_serial.clear()

        # Now add the new ports
        self.ui.comboBox_serial.addItems(utilities.list_serial_ports())

    def exportAction(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog

        filename, _ = QFileDialog.getSaveFileName(self, "Export data to...", "", "JSON Data File (*.json)", options=options)
        if filename:  # user provided a file
            logging.debug(filename)
            with open(filename, 'w') as file:  # write to that file
                file.write(self.component_registry.export())  # serialize the registry

    def importAction(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog

        filename, _ = QFileDialog.getOpenFileName(self, "Import data from...", "", "JSON Data File (*.json);;All files (*)", options=options)
        if filename:  # user provided a file
            logging.debug(filename)
            with open(filename, 'r') as file:  # read from that file
                self.component_registry.infuse(file.read())  # import the file's contents into the registry

        self.postImport()

    def postImport(self):
        """
        Perform some actions after the import, e.g. click buttons
        """
        # As we have reset the components, we need to reset all the component-button assignments
        self.buttonToComponent = {}
        self.componentToButton = {}
        self.linkComponentButtons()

        # Click buttons that need to be clicked
        button: QPushButton
        for button, component in self.buttonToComponent.items():
            if component.active:
                logging.debug("Component " + component.name)
            # Click the button iff the component is active
            button.setChecked(component.active)

        # Update communications values
        self.ui.comms_txpower.setValue(self.component_registry.getSingleComponentValue(TransceiverComponent, 'txPower', 0))

        # Search for the data rate
        dataRate = self.component_registry.getSingleComponentValue(TransceiverComponent, 'dataRate', 0)
        candidates = ['{:.0f} kbps'.format(dataRate), '{:.1f} kbps'.format(dataRate), '{:.0f} Mbps'.format(dataRate / 1e3), '{:.1f} Mbps'.format(dataRate / 1e3)]
        for can in candidates:
            # Search for the data rate text in the list
            index = self.ui.comms_dataRate.findText(can)
            if index != -1:  # the data rate has been found
                self.ui.comms_dataRate.setCurrentIndex(index)  # select it
                break  # don't loop over other candidates

        # Finally, update all the budgets based on the new choices
        self.updateBudgets()

    def updatePeriodic(self):
        """
        Function that gets called every so often to update periodic values in the results
        """
        # Get data from the serial thread, if it exists
        if self.serialThread and self.serialThread.isAlive():
            # Acquire lock, these values should not be changed when reading
            serialThread.dataLock.acquire()

            # Process the data
            receivedData = serialThread.data

            # Since we copied the data, we can release the lock
            serialThread.dataLock.release()

            # Keep a counter of the loop
            iter = 0
            for key, datum in receivedData.items():
                self.sensor_model.setItem(iter, 0, QStandardItem(key))
                self.sensor_model.setItem(iter, 1, QStandardItem("{:.2f}".format(datum)))

                iter += 1

    def updateSerialTXparameters(self):
        """
        Sends the updated TX communications parameters via serial
        """
        serialThread.txParamLock.acquire()

        theoreticalPower = self.component_registry.getSingleComponentValue(TransceiverComponent, 'txPower', 0)
        theoreticalRate = self.component_registry.getSingleComponentValue(TransceiverComponent, 'dataRate', 0)

        print(theoreticalPower)

        if theoreticalPower < 1.5:
            serialThread.txParameters["power"] = 1
        elif theoreticalPower < 5:
            serialThread.txParameters["power"] = 2
        else:
            serialThread.txParameters["power"] = 3

        if theoreticalRate < 320:
            serialThread.txParameters["dataRate"] = 1
        elif theoreticalRate < 1500:
            serialThread.txParameters["dataRate"] = 2
        else:
            serialThread.txParameters["dataRate"] = 3

        serialThread.txParamLock.release()
        serialThread.rewriteParamsEvent.set()  # notify the serial thread that the parameters have been changed

app = QApplication(sys.argv)
w = AppWindow()
w.show()
sys.exit(app.exec_())
