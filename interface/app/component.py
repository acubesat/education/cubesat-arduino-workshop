class Component:
    """
    A satellite component that influences its budget
    """

    # Whether there can be at most one of this type of components
    exclusive = False

    # Whether there shall be at least one of this type of components
    enforced = False

    def __init__(self, subsystem, name):
        self.name = name
        self.displayName = name
        self.subsystem = subsystem
        self.powerPenalty = 0
        self.powerDutyCycle = 1
        self.powerAverage = lambda r: 0  # Will be replaced
        self.costPenalty = 0
        self.linkPenalty = 0
        self.dataPenalty = 0

        # Whether this component has to always be selected by the user
        self.required = False

        # Whether this component has been selected by the user
        self.active = False

        self.unpickle()

    def call(self, attribute, registry=None):
        """
        Get an attribute value. This resolves the attribute if it is a lambda function that requires access to the
        component registry
        """
        if isinstance(attribute, str):
            # The attribute is a string, let's search for it in the component
            value = vars(self)[attribute]
        else:
            # The attribute is just the value, so we return that
            value = attribute

        if callable(value):
            # Value is a function. Call it by providing the registry
            return value(registry)
        else:
            # Value is just a value. We can return it
            return value

    def __powerAverage(self, registry=None):
        """
        Get the average power of a component (in W) based on its duty cycle
        """
        return self.call('powerPenalty', registry) * self.call('powerDutyCycle', registry)

    def constraints(self, registry):
        """
        Check whether the component is compliant with a list of constraints
        :param registry: The ComponentRegistry
        :return: A list of errors, or an empty list if not applicable
        """
        return []  # Override this in your implementation!

    def unpickle(self):
        """
        A function that should be called whenever a pickle is decoded, to take care of some important component
        initialization
        """
        self.powerAverage = self.__powerAverage
