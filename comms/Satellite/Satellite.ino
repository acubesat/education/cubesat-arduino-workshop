#include <SPI.h>
#include "RF24.h"


// Pin defintions
#define TRIG_PIN 5
#define ECHO_PIN 6
#define LDR_PIN  A0
#define TEMP_PIN A1
#define IR_PIN   A2

// NRF24L01 addresses
byte addresses[][8] = {"GndStat", "CubeSat"};


/****************** User Config ***************************/
/* Hardware configuration: Set up nRF24L01 radio on SPI bus plus pins 7 & 8 */
RF24 radio(7,8);
/**********************************************************/

// Variable defination
long echo_duration = 0;
String tx_channel;
struct dataStruct{
  float value;
  int brightness;
  float infrared;
  float temperature;
  int distance;
}Data;

bool c_power = false, c_datarate = false, c_channel = false;

void setup() {

  Serial.begin(115200);
  Serial.println(F("Satellite"));
  
  radio.begin();

  // Set the TX power
  radio.setPALevel(RF24_PA_MIN);

  // Set the data rate
  radio.setDataRate(RF24_2MBPS);

  radio.setChannel(5); // Set the communication channel

  radio.setAutoAck(false);
  
  radio.openWritingPipe(addresses[1]);
  radio.openReadingPipe(1,addresses[0]);
  
  Data.value = 1.22;
  pinMode(TRIG_PIN, OUTPUT); // Sets the TRIG_PIN as an Output
  pinMode(ECHO_PIN, INPUT); // Sets the ECHO_PIN as an Input
  
  radio.startListening(); // Start the radio listening for data
}


void loop() {
  /*******************************************************/
  /********************* OBC Section *********************/
  /*******************************************************/

  /************** Ultrasonic sensor - Start **************/
  digitalWrite(TRIG_PIN, LOW); // Clear the trigger pim
  delayMicroseconds(2); // Wait for the pin to be cleared
  
  // Set the triger pin to HIGH for 10 microseconds to send ultrasonic pulses
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);

  echo_duration = pulseIn(ECHO_PIN, HIGH); // Read the echo from the echo pin immediately after the cessation of transmission
  Data.distance = echo_duration*0.034/2; // Calculating the distance
  
  // Prints the distance
  Serial.print("u");
  Serial.println(Data.distance);
  /*************** Ultrasonic sensor - End ***************/

  /************** Brightness sensor - Start **************/
  Data.brightness = analogRead(LDR_PIN);
  Serial.print("b");
  Serial.println(Data.brightness);
  /*************** Brightness sensor - End ***************/

  /*************** Infrared sensor - Start ***************/
  Data.infrared = analogRead(IR_PIN);
  Serial.print("i");
  Serial.println(Data.infrared);
  /**************** Infrared sensor - End ****************/

  /************* Temperature sensor - Start *************/
  Data.temperature = (analogRead(TEMP_PIN) * 4.88)/10.0;
  Serial.print("t");
  Serial.println(Data.temperature);
  /************** Temperature sensor - End **************/

  
  /*******************************************************/
  /******************** COMMS Section ********************/
  /*******************************************************/
  if (radio.isChipConnected()) {
    radio.stopListening(); // First, stop listening so we can talk.
  
    if (!radio.write( &Data, sizeof(Data) )){
     Serial.println(F("failed"));
    }
    Data.value += 0.01;
  }

  
  delay(500);


/****************** Change Roles via Serial Commands ***************************/

  if ( Serial.available() )
  {
    int i = 0;
    char c = toupper(Serial.read());
    switch (c) {
      case 'P':
        c_power = true;
        break;
      case 'D':
        c_datarate = true;
        break;
      case 'C':
        c_channel = true;
        break;
      default:
        Serial.println(F(" Wrong command given "));
        break;
    }
    
    while (Serial.available()) {
      c = toupper(Serial.read());
      radio.stopListening();
      if ( c == '\n' ) {
        break;
      }
      
      if (c_power) {
        switch (c) {
          case '1':
            radio.setPALevel(RF24_PA_MIN);
            break;
          case '2':
            radio.setPALevel(RF24_PA_LOW);
            break;
          case '3':
            radio.setPALevel(RF24_PA_HIGH);
            break;
        }
        c_power = false;
        Serial.println(F(" TX Poer set "));
      } else if (c_datarate) {
          switch (c) {
            case '1':
              radio.setDataRate(RF24_250KBPS);
              break;
            case '2':
              radio.setDataRate(RF24_1MBPS);
              break;
            case '3':
              radio.setDataRate(RF24_2MBPS);
              break;
          }
        c_datarate = false;
        Serial.println(F(" Data rate set "));
      } else if (c_channel) {
        tx_channel += c;
        if ( i++ > 1 ) {
          radio.setChannel((uint8_t)(tx_channel.toInt()));
          i = 0;
        }
        c_channel = false;
        Serial.println(F(" Channel changed "));
      }
      radio.startListening(); 
    }
  }
}
