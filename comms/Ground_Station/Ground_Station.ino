#include <SPI.h>
#include "RF24.h"

// NRF24L01 addresses
byte addresses[][8] = {"GndStat", "CubeSat"};


/****************** User Config ***************************/

/* Hardware configuration: Set up nRF24L01 radio on SPI bus plus pins 7 & 8 */
RF24 radio(7,8);
/**********************************************************/

struct dataStruct{
  float value;
  int brightness;
  float infrared;
  float temperature;
  int distance;
}Data;

volatile unsigned long total_packet_count = 0, packets_lost = 0;
volatile float previous_value = 0.0f;
volatile bool data_ready = false;

bool c_power = false, c_datarate = false, c_channel = false;
String tx_channel;

void setup() {
  Serial.begin(115200);
  Serial.println(F("Ground Station"));
  
  radio.begin();
  
  // Set the PA Level low to prevent power supply related issues since this is a
  // getting_started sketch, and the likelihood of close proximity of the devices. RF24_PA_MAX is default.
  radio.setPALevel(RF24_PA_MAX);
  radio.setDataRate(RF24_2MBPS);

  radio.setChannel(5); // Set the communication channel
  
  radio.setAutoAck(false);
  
  // Open a writing and reading pipe on each radio, with opposite addresses
  radio.openWritingPipe(addresses[0]);
  radio.openReadingPipe(1,addresses[1]);
  
  Data.value = 1.22;  
  
  radio.startListening(); // Start the radio listening for data
  attachInterrupt(0, check_radio, LOW); // Enable thew interrupt handler for NRF24L01
}

void loop() {
  /*******************************************************/
  /******************** COMMS Section ********************/
  /*******************************************************/
  if ( data_ready ) {
    Serial.print(F("\nError: "));
    Serial.println( ( packets_lost/(float)(total_packet_count) ) * 100.0);
  
    Serial.print(F("\nValues:\n"));
    Serial.print(F("Brightness: "));
    Serial.println(Data.brightness);
  
    Serial.print(F("Infrared: "));
    Serial.println(Data.infrared);
  
    Serial.print(F("Temperature: "));
    Serial.println(Data.temperature);
  
    Serial.print(F("Distance: "));
    Serial.println(Data.distance);

    data_ready = false;
  } else {
    Serial.println(F("No data. "));
  }

  if ( Serial.available() )
  {
    int i = 0;
    char c = toupper(Serial.read());
    switch (c) {
      case 'P':
        c_power = true;
        break;
      case 'D':
        c_datarate = true;
        break;
      case 'C':
        c_channel = true;
        break;
      default:
        Serial.println(F(" Wrong command given "));
        break;
    }
    
    while (Serial.available()) {
      c = toupper(Serial.read());
      radio.stopListening();
      if ( c == '\n' ) {
        break;
      }
      
      if (c_power) {
        switch (c) {
          case '1':
            radio.setPALevel(RF24_PA_MIN);
            break;
          case '2':
            radio.setPALevel(RF24_PA_LOW);
            break;
          case '3':
            radio.setPALevel(RF24_PA_HIGH);
            break;
        }
        c_power = false;
        Serial.println(F(" TX Poer set "));
      } else if (c_datarate) {
          switch (c) {
            case '1':
              radio.setDataRate(RF24_250KBPS);
              break;
            case '2':
              radio.setDataRate(RF24_1MBPS);
              break;
            case '3':
              radio.setDataRate(RF24_2MBPS);
              break;
          }
        c_datarate = false;
        Serial.println(F(" Data rate set "));
      } else if (c_channel) {
        tx_channel += c;
        if ( i++ > 1 ) {
          radio.setChannel((uint8_t)(tx_channel.toInt()));
          i = 0;
        }
        c_channel = false;
        Serial.println(F(" Channel changed "));
      }
      radio.startListening(); 
    }
  }
  
  delay(100);
}

/********************************************************/
/********** Interrupt for radio communications **********/
/********************************************************/
void check_radio(void)                                // Receiver role: Does nothing!  All the work is in IRQ
{
  bool tx,fail,rx;
  radio.whatHappened(tx,fail,rx); // Get the register status to find the interrupt source

  if ( rx ){

    // Corrupt payload has been flushed
    if(radio.getDynamicPayloadSize() < 1){
      return; 
    }
    
    radio.read( &Data, sizeof(Data) ); // Read in the data
    total_packet_count++; // Incement the received packet count

    // Find if any packets have been lost
    if ( ( Data.value != (previous_value + 0.01) ) && previous_value > 0.0 ) {
      packets_lost += (unsigned long)((Data.value - previous_value) * 100);
      total_packet_count += (unsigned long)((Data.value - previous_value) * 100);
   }
   previous_value = Data.value;
   data_ready = true;
  }
}
