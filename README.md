# CubeSat Arduino Workshop

An interactive game to demonstrate engineering-centred **space mission design** on a **3U [CubeSat](http://www.cubesat.org/)**
that allows users to change satellite parameters, and observe the results on
theoretical budgets and real-life Arduino communication.

You don't need any programming or electronics experience to work with this
project. Just change all the values and let your imagination roam free!

Learners can try messing with each parameter to see how it affects their
mission **budgets** and **constraints**. The goal of the game is to produce
the best design possible, which covers all engineering requirements, and
comes with the lowest cost and best budget utilization.

## Directory structure
- [**`/interface`**](interface/) - The desktop interface for budget simulation
- [**`/comms`**](comms/) - A hands-on Arduino demonstration for satellite communications

## Interface

### Download (Windows)
You can download an executable version of the interface from the [**Tags page**](https://gitlab.com/acubesat/education/cubesat-arduino-workshop/-/tags).
No installation, no dependencies and no other work needed!

### Features
The interface is written on [Python 3](https://www.python.org/) with [PyQt5](https://pypi.org/project/PyQt5/).
It has been tested on **Microsoft Windows** and **Linux** (MacOS should also
work, but has not been confirmed).

It provides a number of options, based on which learners can design a space
mission that is compliant with all constraints, and best covers the following:
- Cost Budget
- Power Budget
- Link Budget
- Data Budget

The following subsystems are modelled:
- Payload
- Trajectoy
- Attitude Determination & Control
- Communications
- Electrical Power Supply
- On-Board Computer

### Screenshots
<img alt="Screenshot of the interface with power budget" src="images/screenshot-1.png" width="720px">
<img alt="Screenshot of the interface with data budget" src="images/screenshot-2.png" width="720px">

### Development
In order to work with the interface, you need to have **Python 3** installed.

After you [get the source code](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository),
run the following commands:
```bash
cd interface
pip3 install -r requirements.txt
python3 app/app.py
```

In order to compile an executable for Windows, we are using [PyInstaller](https://pyinstaller.readthedocs.io/en/stable/installation.html).

## Comms Arduino
The Comms-Arduino project consists of two schematics for the [Arduino IDE](https://www.arduino.cc/en/main/software):
- The **satellite** transmitter, which reads data from **sensors** and sends them wirelessly and via Serial (USB)
- The **ground station** receiver, which receives the wireless data sent from the ground station

Both Arduinos work in a plug-and-play fashion, and should start transmitting
and receiving as soon as their components are connected.

Wireless communication is performed through the **nRF24L01+** 2.4 GHz module,
which can be found for < 1€ on different suppliers.

### Components
The following basic components are used for each transmitter-receiver pair:
- 2 [**Arduino UNOs**](https://store.arduino.cc/arduino-uno-rev3) (or compatible boards)
- 2x [**nRF24L01+**](https://www.sparkfun.com/datasheets/Components/SMD/nRF24L01Pluss_Preliminary_Product_Specification_v1_0.pdf) wireless transceivers
- A 2-pin **photoresistor** (for optical brightness measurements)
- A 2-pin **infrared photoresistor** (for infrared "thermal" measurements)
- An **HC-SR04** ultrasonic distance sensor
- An [**LM35**](http://www.ti.com/lit/ds/symlink/lm35.pdf)
- The corresponding wires and resistors

### Schematics
*Note: You don't need to have all components corrected, the project is expected to operate
even with a number of the components*
#### Satellite (transmitter)
<img alt="Satellite schematic" src="images/schematic-tx.png" width="512px">

#### Ground Station (receiver)
<img alt="Satellite schematic" src="images/schematic-rx.png" width="512px">